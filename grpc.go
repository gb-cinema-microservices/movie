package main

import (
	"context"
	pb "gb-cinema-movie/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Service struct {
	*pb.UnimplementedMovieStorageServer
}

func (s *Service) GetMovieById(ctx context.Context, req *pb.GetMovieByIdRequest) (resp *pb.GetMovieByIdResponse, err error) {
	const sql = `SELECT id, name, poster, url, paid, release_year, genre FROM movies WHERE id = $1 LIMIT 1;`
	resp = &pb.GetMovieByIdResponse{}
	err = Postgres.QueryRow(ctx, sql, req.MovieId).Scan(
		&resp.Id,
		&resp.Name,
		&resp.Poster,
		&resp.Url,
		&resp.IsPaid,
		&resp.ReleaseYear,
		&resp.Genre,
	)
	if err != nil {
		err = status.Errorf(codes.NotFound, "SQL Error: %v", err)
	}
	return resp, err
}

func (s *Service) GetMovieCount(ctx context.Context, req *pb.GetMovieCountRequest) (resp *pb.GetMovieCountResponse, err error) {
	// TODO: при высокой нагрузке отказаться от COUNT(), перенеся цифру в отдельное поле отдельной таблички, обновляя его каким-то методом или триггером в бд
	const sql = `SELECT COUNT(id) FROM movies;`
	resp = &pb.GetMovieCountResponse{}
	err = Postgres.QueryRow(ctx, sql).Scan(&resp.Count)
	if err != nil {
		err = status.Errorf(codes.NotFound, "SQL Error: %v", err)
	}
	return resp, err
}

func (s *Service) GetMovies(ctx context.Context, req *pb.GetMoviesRequest) (resp *pb.GetMoviesResponse, err error) {
	const sql = `SELECT id, name, poster, url, paid, release_year, genre FROM movies LIMIT $1 OFFSET $2;`
	rows, err := Postgres.Query(ctx, sql, req.Limit, req.Offset)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "SQL Error - failed to query data: %v", err)
	}
	defer rows.Close()

	resp = &pb.GetMoviesResponse{}
	for rows.Next() {
		movie := &pb.GetMovieByIdResponse{}
		err = rows.Scan(&movie.Id, &movie.Name, &movie.Poster, &movie.Url, &movie.IsPaid, &movie.ReleaseYear, &movie.Genre)
		if err != nil {
			return nil, status.Errorf(codes.NotFound, "SQL Error - failed to scan row: %v", err)
		}
		resp.Movies = append(resp.Movies, movie)
	}

	if rows.Err() != nil {
		return nil, status.Errorf(codes.NotFound, "SQL Error - failed to read response: %v", err)
	}

	return resp, nil
}

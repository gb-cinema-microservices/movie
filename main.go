package main

import (
	"context"
	pb "gb-cinema-movie/api"
	"google.golang.org/grpc"
	"log"
	"net"
)

const DatabaseAddr = "postgres://movie:secret@elpy.bmrng.net:5432/movie"
const ListenAddr = ":8081"

func main() {
	// DB:
	ctx := context.Background()
	Postgres = ConnectPostgres(ctx, DatabaseAddr)
	defer Postgres.Close()

	// gRPC API:
	rpc := grpc.NewServer()
	pb.RegisterMovieStorageServer(rpc, &Service{})
	listener, err := net.Listen("tcp", ListenAddr)
	if err != nil {
		log.Fatalf("Failed to listen TCP port: %v", err)
	}
	log.Println("Starting gRPC server on", ListenAddr)
	err = rpc.Serve(listener)
	if err != nil {
		log.Fatalf("Failed to serve gRPC: %v", err)
	}
}
